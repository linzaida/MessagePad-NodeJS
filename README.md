#MsgPad-NodeJS

- NodeJS+Sails+MongoDB 做的十分十分十分简单的例子(留言板)。
- 前面使用expressjs作为MVC框架，有同学说换sailsjs，然后就尝试了一下。
- 例子中没有路由的配置，因为sails有blurptint自动路由。
- 静态文件的引用，和expressjs只有路径不同(assettt和public)。
- MongoDB数据库的连接不管自己手动管理，只需要在config/connections.js 和 models.js 中配置就可以用了，
添加，查询，删除数据都很简单 Model.create, Model.destroy, Model.find 直接使用(要注意的是，这里的Model是模型名称，
也就是在 Models 文件夹下添加文件，比如在 api/Models 文件夹下有文件 User.js，那么在 controller中使用就是 User.creae 等。
测试的时候，看官方文档直接写的Model，我也写成Model，在这里搞了好久才明白要写成实体名(刚接触MVC，请不要贱笑)。)
而在express中，我是搬运编写了数据库的那部分代码（因为也只接触了半天，不知道是不是express中也有类似的处理）。
- 界面还是使用了Bootstrap的CSS，jQuery的ajax和DOM操作，
视图模板使用的是EJS(不过。。这个是废话，在这个例子里面，没有用到任何真正模板相关的东西，
最多就是用到了sails提供的layout。这个layout的使用是在config/views.js 里面配置的。
我刚开始没有注意到，直接在index.ejs模板文件中写了\<html>\<head>这些东西，包括引用css和js，
后来检查才发现页面中自动引用asset下面的js文件，css文件不清楚，我想如果assets/styles目录下有其它css文件，应该也会
自动引用的)。

## RESTful 的支持
终于搞懂了Sails的RESTful。理解了就觉得简单了好多（当然，我是说使用起来）。
1. 创建数据模型  Message.js
2. 创建控制器 MessageController.js，这个控制器文件是必须的，不能省略，里面可以只有 module.exports={} 这样一句
3. 在前台通过自动form的method(或者指定jQuery的ajax参数type)为post，get，delete，put就可以了

在这个例子中，没有写put的请求。如果要添加，请这样发送:

```
$.ajax({
  url: '/message/'+id,
  type: 'put',
  data: data
})
```
> 这里的url添加一个参数id，用于让Sails去查找要更新的数据对象。

## 东西很简单，不过对需要学习入门的同学来说，应该还是可以借鉴一下的。
